﻿using APISample.DAL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace APISample.DAL.Repositories
{
    public class PokemonRepository : BaseRepository<Pokemon>
    {
        public override bool Delete(int id)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                Pokemon toRemove = dc.Pokemons.FirstOrDefault(p => p.Id == id);
                dc.Pokemons.Remove(toRemove);
                return dc.SaveChanges() == 1;
            }
        }

        public override Pokemon Get(int id)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                //return dc.Pokemons.FirstOrDefault(p => p.Id == id);
                return dc.Pokemons.Find(id);
            }
        }

        public override IEnumerable<Pokemon> GetAll()
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                return dc.Pokemons.ToList();
            }
        }

        public override int Insert(Pokemon entity)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                dc.Pokemons.Add(entity);
                dc.SaveChanges();
                return entity.Id;
            }
        }

        public override bool Update(Pokemon entity)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                dc.Entry(entity).State = EntityState.Modified;
                return dc.SaveChanges() == 1;
            }
        }
    }
}
