﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APISample.DAL.EF;
using System.Data.SqlClient;

namespace APISample.DAL.Repositories
{
    public class TypeRepository : BaseRepository<EF.Type>
    {
        public override bool Delete(int id)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                EF.Type toRemove = dc.Types.Find(id);
                dc.Types.Remove(toRemove);
                return dc.SaveChanges() == 1;
            }
        }

        public override EF.Type Get(int id)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                return dc.Types.Find(id);
            }
        }

        public override IEnumerable<EF.Type> GetAll()
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                return dc.Types.ToList();
            }
        }

        public override int Insert(EF.Type entity)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                dc.Types.Add(entity);
                dc.SaveChanges();
                return entity.Id;
            }
        }

        public override bool Update(EF.Type entity)
        {
            using (PokemonEntities dc = new PokemonEntities())
            {
                dc.Entry(entity).State = EntityState.Modified;
                return dc.SaveChanges() == 1;
            }
        }
    }
}
