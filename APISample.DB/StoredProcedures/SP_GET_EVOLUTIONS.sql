﻿CREATE PROCEDURE [dbo].[SP_GET_EVOLUTIONS]
	@id int
AS
	WITH temp_table(
		Id, 
		Number, 
		[Name], 
		Type1Id, 
		Type2Id, 
		Picture, 
		PrevolutionId,
		Rang
	) AS (
		SELECT *, 0 FROM Pokemon WHERE Id = @id
		UNION ALL
		SELECT p1.*, p2.Rang + 1 FROM Pokemon p1
		JOIN temp_table p2 ON p2.Id = p1.PrevolutionId
	) SELECT * FROM temp_table WHERE Id <> @id
RETURN 0
