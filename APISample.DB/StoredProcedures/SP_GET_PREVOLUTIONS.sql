﻿CREATE PROCEDURE [dbo].[SP_GET_PREVOLUTIONS]
@id int
AS
	WITH temp_table(
		Id, 
		Number, 
		[Name], 
		Type1Id, 
		Type2Id, 
		Picture, 
		PrevolutionId
	) AS (
		SELECT * FROM Pokemon WHERE Id = @id
		UNION ALL
		SELECT p1.* FROM Pokemon p1
		JOIN temp_table p2 ON p2.PrevolutionId = p1.Id
	) SELECT * FROM temp_table WHERE Id <> @id
RETURN 0
