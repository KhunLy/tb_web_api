﻿using APISample.DAL.Repositories;
using APISample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ToolBox.Mapper;

namespace APISample.Controllers
{
    public class PokemonController : ApiController
    {
        public IEnumerable<PokemonDTO> Get()
        {
            PokemonRepository repo = new PokemonRepository();
            return repo.GetAll().Select(t => t.Map<PokemonDTO>());
        }

        public PokemonDTO Get(int id)
        {
            PokemonRepository repo = new PokemonRepository();
            PokemonDTO t = repo.Get(id)?.Map<PokemonDTO>();
            if (t == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return t;
        }

        public int Post(PokemonDTO t)
        {
            if (ModelState.IsValid)
            {
                PokemonRepository repo = new PokemonRepository();
                return repo.Insert(t.Map<DAL.EF.Pokemon>());
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }

        public bool Delete(int id)
        {
            PokemonRepository repo = new PokemonRepository();
            try
            {
                return repo.Delete(id);
            }
            catch (Exception)
            {

                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

        }

        public bool Put(int id, PokemonDTO t)
        {
            PokemonRepository repo = new PokemonRepository();
            if (ModelState.IsValid)
            {
                try
                {
                    t.Id = id;
                    return repo.Update(t.Map<DAL.EF.Pokemon>());
                }
                catch (Exception)
                {

                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }
    }
}
