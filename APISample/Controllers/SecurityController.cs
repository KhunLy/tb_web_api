﻿using APISample.Models;
using APISample.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace APISample.Controllers
{
    public class SecurityController : ApiController
    {
        [Route("api/login")]
        [HttpPost]
        public string Login(LoginModel model)
        {
            // valider avec la db et vérifier si l'utilisateur existe
            // fournir un token le cas échéans
            TokenService service = new TokenService();
            return service.GenerateToken(new User
                { Email = model.Username, Role = "ADMIN" }
            );
        }
    }
}
