﻿using APISample.CustomAttributes;
using APISample.DAL.Repositories;
using APISample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ToolBox.Mapper;

namespace APISample.Controllers
{
    public class TypeController : ApiController
    {
        public IEnumerable<TypeDTO> Get()
        {
            TypeRepository repo = new TypeRepository();
            return repo.GetAll().Select(t => t.Map<TypeDTO>());
        }

        public TypeDTO Get(int id)
        {
            TypeRepository repo = new TypeRepository();
            TypeDTO t = repo.Get(id)?.Map<TypeDTO>();
            if(t == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return t;
        }

        [ApiAuthorization]
        public int Post(TypeDTO t)
        {
            if (ModelState.IsValid)
            {
                TypeRepository repo = new TypeRepository();
                return repo.Insert(t.Map<DAL.EF.Type>());
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }

        public bool Delete(int id)
        {
            TypeRepository repo = new TypeRepository();
            try
            {
                return repo.Delete(id);
            }
            catch (Exception)
            {

                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            
        }

        public bool Put(int id, TypeDTO t)
        {
            TypeRepository repo = new TypeRepository();
            if (ModelState.IsValid)
            {
                try
                {
                    t.Id = id;
                    return repo.Update(t.Map<DAL.EF.Type>());
                }
                catch (Exception)
                {

                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
            }
            throw new HttpResponseException(HttpStatusCode.BadRequest);
        }
    }
}
