﻿using APISample.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace APISample.CustomAttributes
{
    public class ApiAuthorizationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues(
                "Authorization", out IEnumerable<string> values);
            string token 
                = values?.FirstOrDefault(val => val.StartsWith("Bearer"))
                ?.Replace("Bearer ", string.Empty);
            if(token == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            TokenService service = new TokenService();
            ClaimsPrincipal claims = service.ValidateToken(token);
            if(claims == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }
    }
}