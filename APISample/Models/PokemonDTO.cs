﻿using APISample.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APISample.Models
{
    public class PokemonDTO
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public byte[] Picture { get; set; }
        private string _Type1;
        public string Type1
        {
            get
            {
                if (_Type1 == null)
                {
                    TypeRepository repo = new TypeRepository();
                    _Type1 = repo.Get(Type1Id).Name; 
                }
                return _Type1;
            }
        }
        private string _Type2;
        public string Type2
        {
            get { return _Type2 = _Type2 ?? (new TypeRepository()).Get(Type2Id ?? 0)?.Name; }
        }
        public int Type1Id { get; set; }
        public int? Type2Id { get; set; }

    }
}