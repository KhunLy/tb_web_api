﻿using APISample.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace APISample.Utils
{
    public class TokenService
    {
        private JwtSecurityTokenHandler _Handler;
        private SymmetricSecurityKey _SecurityKey;
        private string _Signature = "j'ai besoin d'une signature suffisament longue, même très très longue";

        public TokenService()
        {
            _Handler = new JwtSecurityTokenHandler();
            _SecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_Signature));
        }

        public string GenerateToken(User user)
        {
            SigningCredentials credentials = new SigningCredentials(
                _SecurityKey, SecurityAlgorithms.HmacSha256
            );
            JwtSecurityToken token = new JwtSecurityToken(
                "khunly.be",
                "monAppliIonic",
                new List<Claim> { new Claim("email", user.Email), new Claim("role", user.Role) },
                DateTime.Now,
                DateTime.Now.AddDays(1),
                credentials
            );
            return _Handler.WriteToken(token);
        }

        public ClaimsPrincipal ValidateToken(string token)
        {
            return _Handler.ValidateToken(token, new TokenValidationParameters {
                ValidateLifetime = true,
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidIssuer = "khunly.be",
                ValidAudience = "monAppliIonic",
                RequireSignedTokens  = true,
                IssuerSigningKey = _SecurityKey
            }, out SecurityToken validatedToken);
        }
    }
}